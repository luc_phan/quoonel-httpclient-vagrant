class this_project::config inherits this_project {
  file { '/home/vagrant/.bashrc':
    require  => Exec['virtualenvwrapper'],
    source   => 'puppet:///modules/this_project/_bashrc',
    owner    => vagrant,
    group    => vagrant,
    mode     => 644
  }
}
