class this_project::install inherits this_project {
  yumrepo { 'IUS':
    baseurl => "http://dl.iuscommunity.org/pub/ius/stable/$operatingsystem/$operatingsystemmajrelease/$architecture",
    descr => 'IUS Community repository',
    enabled => 1,
    gpgcheck => 0
  }
  ->
  package { 'python34u':
    ensure => 'installed'
  }
  ->
  exec { 'virtualenv':
    command => 'pip3 install virtualenv',
    path    => '/usr/bin'
  }
  ->
  exec { 'virtualenvwrapper':
    command => 'pip3 install virtualenvwrapper',
    path    => '/usr/bin'
  }
  ->
  exec { 'local virtualenv':
    require  => File['/home/vagrant/.bashrc'],
    command  => 'source /home/vagrant/.bashrc && mkvirtualenv local -p /usr/bin/python3',
    provider => 'shell',
    user     => vagrant,
    creates  => '/home/vagrant/.virtualenvs/local'
  }
}
