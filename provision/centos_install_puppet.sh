#!/bin/env sh

#set -x

if which puppet > /dev/null 2>&1; then
  echo "Puppet is already installed"
else
  yum install -y puppet
fi

puppet module install saz-timezone
